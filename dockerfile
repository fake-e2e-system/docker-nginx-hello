FROM nginx
RUN mkdir /var/helloworld
COPY /files/nginx.conf /etc/nginx/nginx.conf
COPY /files/index.html /var/helloworld/index.html
